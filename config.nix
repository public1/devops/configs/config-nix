{
  packageOverrides = pkgs: with pkgs; {
    # Run `nix-env -iA nixpkgs.myPackages` to update nix packages.
    myPackages = pkgs.buildEnv {
      name = "my-packages";
      paths = [
        # neovim # Installing neovim-nightly manually in Ansible.
        # TODO: Figure out how to do this in Nix and remove Ansible stuff..
        # Lazyvim
        tree-sitter
        nodejs_21
        lazygit
        cppcheck
        markdownlint-cli
        fd
        ripgrep
        rustup # neocmake needed cargo so installing rustup, which installs cargo.
        deno # For peek.nvim
        # OhMyZsh
        autojump
        starship
        zoxide
        # Utilities
        fzf
        tmux
        colordiff
        tldr # Alternative man pages.
        xclip # Manage the clipboard.
        delta # Better diffing in the terminal.
        difftastic # Another diff tool.
        lnav # TUI for viewing logs.
        bat # A better cat.
        grc # Generic terminal output colouriser.
        bear # Compile commands generator.
      ];
    };
  };
}
