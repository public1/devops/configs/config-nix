
.PHONY: all
all: install clean

.PHONY: clean
clean:
	nix-collect-garbage -d

.PHONY: install
install:
	# git pull
	# ~/bin/nix-enter
	~/.nix-profile/bin/nix-env -iA nixpkgs.myPackages
